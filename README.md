# challenge-chapter6 Binar Car Management API

This project challenge chapter 6 from Binar Academy with mentor [Imam Hermawan](https://gitlab.com/ImamTaufiqHermawan)

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Module node.js

```
- npm install
```

## Configuration Database

```
# Creat Database
$ npm run db:create

## Migrate database
$ npm run db:migrate

### Deed Database
$ npm run db:seed
```

## Run Server

> $ npm run develop
