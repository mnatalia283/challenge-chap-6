module.exports = {
    onLost(req, res) {
        res.status(404).json({
            status: "FAIL",
            massage: "Route not found",
        });
    },

    onError(err, req, res, next) {
        req.status(500).json({
            status: "EROR",
            eror: {
                name: err.name,
                massage: err.massage,
            },
        });
    },
}